﻿using Microsoft.EntityFrameworkCore;
using Recipes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Recipes.Persistense
{
    internal class RecipesRepository
    {
        RecipeContext context;

        public RecipesRepository(RecipeContext context)
        {
            this.context = context;
        }

        public IEnumerable<Recipe> GetAllRecipes()
        {
            return this.context.Recipes
                .Include(r => r.Ingredients)
                .ToList();
        }

        public IEnumerable<Recipe> GetRecipes(Expression<Func<Recipe, bool>> predicate)
        {
            return this.context.Recipes
                .Include(r => r.Ingredients)
                .Where(predicate)
                .ToList();
        }

        public IEnumerable<Recipe> GetRecipesContaining(string keyword)
        {
            return this.context.Recipes
                .Include(r => r.Ingredients)
                .Where(r => r.Name.ToUpper().Contains(keyword.ToUpper()))
                .ToList();
        }
    }
}
