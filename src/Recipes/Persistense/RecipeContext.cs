using Microsoft.EntityFrameworkCore;
using Recipes.Models;

namespace Recipes.Persistense
{
    public class RecipeContext : DbContext
    {
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Recipe> Recipes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        {
            optionsBuilder.UseSqlite("Data Source=recipes.db");
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    //modelBuilder.Entity<RecipeIngredient>()
        //    //    .HasKey(ri => new { ri.RecipeId, ri.IngredientId });

        //    // modelBuilder.Entity<IngredientRecipe>()
        //    //     .HasOne(ir => ir.Ingredient)
        //    //     .WithMany(i => i.IngredientRecipes)
        //    //     .HasForeignKey(ir => ir.IngredientId);

        //    // modelBuilder.Entity<IngredientReeipe>()
        //    //     .HasOne(ir => ir.Recipe)
        //    //     .WithMany(r => r.IngredientRecipes)
        //    //     .HasForeignKey(ir => ir.RecipeId);
        //}
        
    }
}