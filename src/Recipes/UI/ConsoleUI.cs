﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Recipes.Models;
using Recipes.Persistense;

namespace Recipes.UI
{
    public class ConsoleUI
    {
        private readonly Dictionary<Dialog, Action> ConsoleDialogs = new Dictionary<Dialog, Action>();
        private readonly RecipeContext context;
        private readonly RecipesRepository recipesRepository;

        public ConsoleUI()
        {
            this.context = new RecipeContext();
            this.recipesRepository = new RecipesRepository(this.context);
        }

        public bool IsRunning { get; set; }

        public void Run()
        {
            this.IsRunning = true;
            this.SetupDialogs();
            this.MainDialog();
        }

        private void SetupDialogs()
        {
            this.ConsoleDialogs.Add(Dialog.Close, this.TearDownDialog);
            this.ConsoleDialogs.Add(Dialog.New, this.NewRecipeDialog);
            this.ConsoleDialogs.Add(Dialog.Search, this.SearchRecipeDialog);
            this.ConsoleDialogs.Add(Dialog.All, this.ShowAllRecipesDialog);
            this.ConsoleDialogs.Add(Dialog.SomethingElse, this.RandomChatDialog);
        }

        private void MainDialog()
        {
            while (this.IsRunning)
            {
                Console.WriteLine();
                Console.WriteLine("What do you wanna do?");

                Console.WriteLine(@"1) New Recipe");
                Console.WriteLine(@"2) Search Recipe");
                Console.WriteLine(@"3) Show All");
                Console.WriteLine(@"4) Something else");
                Console.WriteLine(@"0) Nothing!");

                int choice = int.Parse(Console.ReadLine());
                this.ConsoleDialogs[(Dialog)choice].Invoke();
            }
        }

        private void TearDownDialog()
        {
            Console.WriteLine("Suit yourself!");
            this.IsRunning = false;
        }

        private void NewRecipeDialog()
        {
            Console.WriteLine("Ok let's do it!");

            var name = AskForNameOfRecipe();
            var ingredients = AskForIngredients();
            var steps = AskForSteps();

            var recipe = new Recipe
            {
                Name = name,
                Ingredients = ingredients.ToList(),
                Steps = steps
            };

            this.SaveNewRecipe(recipe);
        }

        private void SaveNewRecipe(Recipe recipe)
        {
            this.context.Recipes.Add(recipe);
            this.context.SaveChanges();
        }

        private void SearchRecipeDialog()
        {
            Console.WriteLine();
            Console.WriteLine("What are you looking for?");
            var keyword = Console.ReadLine();

            var matchedRecipes = this.recipesRepository.GetRecipesContaining(keyword);

            this.WriteRecipesToConsole(matchedRecipes);
        }

        private void WriteRecipesToConsole(IEnumerable<Recipe> recipes)
        {
            if (!recipes.Any())
            {
                Console.WriteLine("Didn't find anything!");
                return;
            }

            foreach (var recipe in recipes)
            {
                this.WriteRecipeToConsole(recipe);
            }
        }

        private void WriteRecipeToConsole(Recipe recipe)
        {
            Console.WriteLine();
            Console.WriteLine($"Recipe: {recipe.Name}");
            Console.WriteLine("     Ingredients: ");
            foreach (var ingredient in recipe.Ingredients)
            {
                var unit = ingredient.Unit != Unit.x ? ingredient.Unit.ToString() : string.Empty;
                Console.WriteLine($"          {ingredient.Amount}{unit} {ingredient.Name}(s)");
            }

            Console.WriteLine("     Steps: ");
            Console.WriteLine($"          {recipe.Steps}");
        }

        private void RandomChatDialog()
        {
            throw new NotImplementedException();
        }

        private void ShowAllRecipesDialog()
        {
            var recipes = this.recipesRepository.GetAllRecipes();

            foreach (var recipe in recipes)
            {
                this.WriteRecipeToConsole(recipe);
            }
        }

        private string AskForNameOfRecipe()
        {
            Console.WriteLine("What's the name of it?");
            var name = Console.ReadLine();
            return name;
        }

        private IEnumerable<Ingredient> AskForIngredients()
        {
            Console.WriteLine("Put in ingredients like this: onion:2:x,sugar:30:mg s.o.");
            var ingredients = this.ExtractIngredientsFromUserInput(Console.ReadLine());
            return ingredients;
        }

        private string AskForSteps()
        {
            Console.WriteLine("Add the steps:");
            var steps = Console.ReadLine();
            return steps;
        }

        private IEnumerable<Ingredient> ExtractIngredientsFromUserInput(string userInput)
        {
            var ingredients = new List<Ingredient>();
            var eachIngredients = userInput.Split(',');

            foreach (var item in eachIngredients)
            {
                var ingredientNameAndAmount = item.Split(':');

                var ingredient = new Ingredient
                {
                    Name = ingredientNameAndAmount[0],
                    Amount = int.Parse(ingredientNameAndAmount[1]),
                    Unit = Enum.Parse<Unit>(ingredientNameAndAmount[2])
                };

                ingredients.Add(ingredient);
            }

            return ingredients;
        }
    }
}
