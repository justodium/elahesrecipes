﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recipes.UI
{
    public enum Dialog
    {
        Close,
        New,
        Search,
        All,
        SomethingElse
    }
}
