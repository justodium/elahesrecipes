﻿using Recipes.UI;

namespace Recipes
{
    class Program
    {
        static void Main(string[] args)
        {
            var consoleServices = new ConsoleUI();
            consoleServices.Run();
        }
    }
}
