using System.Collections.Generic;

namespace Recipes.Models
{
    public class Recipe
    {
        public int RecipeId { get; set; }
        public string Name { get; set; }
        public string Steps { get; set; }
        public ICollection<Ingredient> Ingredients { get; set; } = new List<Ingredient>();     
    }
}