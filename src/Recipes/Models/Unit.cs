namespace Recipes.Models
{
    public enum Unit
    {
        x,
        mg,
        g,
        ml,
        l,
        Spoon,
        TeaSpoon
    }
}