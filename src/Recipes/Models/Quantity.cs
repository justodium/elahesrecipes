namespace Recipes.Models
{
    public class Quantity
    {
        public int QuantityId { get; set; }
        public int Value { get; set; }
        public Unit Unit { get; set; }
        
        public int RecipeId { get; set; }
        public Recipe Recipe { get; set; }
    }
}